
import raw from "../../middleware/route.async.wrapper.mjs";
import user_model from "./user.model.mjs";
import express from 'express';
import pagination_router from "./pagination.router.mjs"
import yup_validator from '../../middleware/validateBody.mjs';

const router = express.Router();
router.use(express.json())
router.use('/pagination', pagination_router);


// CREATES A NEW USER
router.post("/",yup_validator,raw( async (req, res) => {
    const user = await user_model.create(req.body);
    res.status(200).json(user);
}) );

// GET ALL USERS
router.get( "/",raw(async (req, res) => {
    const users = await user_model.find()
                                  // .select(`-__v`);
                                  .select(`-_id 
                                          first_name 
                                          last_name 
                                          email 
                                          phone`);
    res.status(200).json(users);
  })  
);

// GETS A SINGLE USER
router.get("/:id",raw(async (req, res) => {
    const user = await user_model.findById(req.params.id)
                                    // .select(`-_id 
                                    //     first_name 
                                    //     last_name 
                                    //     email
                                    //     phone`);
    if (!user) return res.status(404).json({ status: "No user found." });
    res.status(200).json(user);
  })
);
// UPDATES A SINGLE USER
router.put("/:id",yup_validator,raw(async (req, res) => {
    const user = await user_model.findByIdAndUpdate(req.params.id,req.body, 
                                                    {new: true, upsert: false });
    res.status(200).json(user);
  })
);


// DELETES A USER
router.delete("/:id",raw(async (req, res) => {
    const user = await user_model.findByIdAndRemove(req.params.id);
    if (!user) return res.status(404).json({ status: "No user found." });
    res.status(200).json(user);
  })
);


export default router;
