
import express from 'express';
import raw from "../../middleware/route.async.wrapper.mjs";
import user_model from "./user.model.mjs";
const pagination_router = express.Router();
pagination_router.use(express.json())


pagination_router.get("/:page/:resultsNumber",raw(async (req, res) => {
    console.log("in pagination page user");
    const user = await user_model.find().skip(Number(req.params.page) * Number(req.params.resultsNumber)).limit(Number(req.params.resultsNumber));
                                                    
    res.status(200).json(user);
  })
);  
export default pagination_router;
